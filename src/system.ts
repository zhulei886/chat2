import type { Model } from "./types"

export const defaultSetting = {
  continuousDialogue: true,
  archiveSession: false,
  openaiAPIKey: "",
  openaiAPITemperature: 60,
  password: "",
  systemRule: "",
  model: "gpt-3.5-turbo" as Model
}
//
// export const defaultMessage = `
// - 如果本项目对你有所帮助，可以给小猫 [买点零食](/zsm.jpg)。
// - 感谢 [Diu](https://github.com/ddiu8081) | [ourongxing](https://github.com/ourongxing)
// - [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部。
// - 可填入自己的 KEY 使用更流畅。
// - 欢迎老板打赏~ 赞赏会用于购买 apikey 维持本站的运转，apikey 成本巨增😭，大家可以用自己的 key 使用本站，自己的 key 成本很低。`

export const defaultMessage = `
- 大家好，我是宙斯AI智能聊天机器人，基于最新语言模型的AI人工智能聊天机器人，为你带来AI聊天、翻译、创作、编程、撰写文案、营销策划、法律咨询、知识学习等等，提交学习工作效率！
- [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部
`

export type Setting = typeof defaultSetting

export const defaultResetContinuousDialogue = false

export const defaultMaxInputTokens: Record<Model, number> = {
  "gpt-3.5-turbo": 3072,
  "gpt-4": 6144,
  "gpt-4-32k": 24576
}

export const defaultModel: Model = "gpt-3.5-turbo"
